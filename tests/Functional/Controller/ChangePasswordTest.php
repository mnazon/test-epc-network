<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\DataFixtures\UserFixtures;
use App\Tests\Functional\BaseApiTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ChangePasswordTest extends BaseApiTestCase
{
    private const URI = '/api/users/change_password';

    public function testChangePasswordForNotAuthUser(): void
    {
        $this->client->request(Request::METHOD_POST, self::URI, [
            'json' => [
                'password' => 'pass',
                'newPassword' => 'newPass',
            ]
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
    }

    public function testChangePasswordSuccess(): void
    {
        $this->loadFixtures([new UserFixtures()]);
        $this->authenticateClient(UserFixtures::REFERENCE_USER, UserFixtures::DEFAULT_PASSWORD);

        $response = $this->client->request(Request::METHOD_POST, self::URI, [
            'json' => [
                'password' => UserFixtures::DEFAULT_PASSWORD,
                'newPassword' => 'newPassword',
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('token', $response->toArray(false));

        //assert that the old token has expired
        $this->client->request(Request::METHOD_POST, self::URI, [
            'json' => [
                'password' => UserFixtures::DEFAULT_PASSWORD,
                'newPassword' => 'newPassword',
            ]
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
    }

    public function testChangePasswordWithWrongPassword(): void
    {
        $this->loadFixtures([new UserFixtures()]);
        $this->authenticateClient(UserFixtures::REFERENCE_USER, UserFixtures::DEFAULT_PASSWORD);

        $response = $this->client->request(Request::METHOD_POST, self::URI, [
            'json' => [
                'password' => 'wrongPassword',
                'newPassword' => 'newPassword',
            ]
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertMatchesPattern([
            '@context' => '/api/contexts/ConstraintViolationList',
            '@type' => 'ConstraintViolationList',
            'hydra:title' => 'An error occurred',
            'hydra:description' => '@string@',
            'violations' => '@array@.count(1)',
        ], $response->toArray(false));

        $this->assertJsonContains([
            'hydra:description' => 'password: Wrong value for current password',
        ]);
    }
}
