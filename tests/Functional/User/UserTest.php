<?php

declare(strict_types=1);

namespace App\Tests\Functional\User;

use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Tests\Functional\BaseApiTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserTest extends BaseApiTestCase
{
    private const URI = '/api/users';

    public function testCreateUser(): void
    {
        $this->client->request(Request::METHOD_POST, self::URI, [
            'json' => [
                'username' => 'example_username',
                'password' => 'password',
                'firstName' => 'FirstName',
                'lastName' => 'LastName',
            ]
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
        $this->assertMatchesResourceItemJsonSchema(User::class);
    }

    public function testUpdateUser(): void
    {
        $this->loadFixtures([new UserFixtures()]);
        $this->authenticateClient(UserFixtures::REFERENCE_USER, UserFixtures::DEFAULT_PASSWORD);
        $iri = $this->findIriBy(User::class, ['username' => UserFixtures::REFERENCE_USER]);

        $this->client->request(Request::METHOD_PUT, $iri, [
            'json' => [
                'firstName' => 'NewFirstName',
                'lastName' => 'NewLastName'
            ]
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertMatchesResourceItemJsonSchema(User::class);
        $this->assertJsonContains([
            'firstName' => 'NewFirstName',
            'lastName' => 'NewLastName',
        ]);
    }
}
