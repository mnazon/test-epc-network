<?php

declare(strict_types=1);

namespace App\Tests\Functional\Auth;

use App\DataFixtures\UserFixtures;
use App\Tests\Functional\BaseApiTestCase;
use Symfony\Component\HttpFoundation\Request;

class AuthTest extends BaseApiTestCase
{
    public const AUTH_URI = '/authentication_token';

    public function testSuccessLogin(): void
    {
        $this->loadFixtures([new UserFixtures()]);

        $this->client->request(Request::METHOD_POST, self::AUTH_URI, ['json' => [
            'username' => UserFixtures::REFERENCE_USER,
            'password' => UserFixtures::DEFAULT_PASSWORD,
        ]]);

        $this->assertResponseIsSuccessful();
    }
}
