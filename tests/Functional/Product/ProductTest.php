<?php

declare(strict_types=1);

namespace App\Tests\Functional\Product;

use App\DataFixtures\ProductFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Product;
use App\Tests\Functional\BaseApiTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductTest extends BaseApiTestCase
{
    private const URI = '/api/products';

    public function afterSetUp(): void
    {
        $this->loadFixtures([new UserFixtures(), new ProductFixtures()]);
    }

    public function testCreateProduct(): void
    {
        $this->authenticateClient(UserFixtures::REFERENCE_USER, UserFixtures::DEFAULT_PASSWORD);

        $response = $this->client->request(Request::METHOD_POST, self::URI, [
            'json' => [
                'name' => 'Super product',
                'price' => 100,
            ]
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
        $this->assertMatchesPattern([
            '@context' => '/api/contexts/Product',
            '@id' => '@string@',
            '@type' => 'Product',
            'name' => '@string@',
            'price' => '@integer@',
        ], $response->toArray());
    }

    public function testList(): void
    {
        $this->authenticateClient(UserFixtures::REFERENCE_USER, UserFixtures::DEFAULT_PASSWORD);

        $this->client->request(Request::METHOD_GET, self::URI);
        $this->assertMatchesResourceCollectionJsonSchema(Product::class);
        $this->assertResponseIsSuccessful();
    }

    public function testUpdateProduct(): void
    {
        $this->authenticateClient(UserFixtures::REFERENCE_USER, UserFixtures::DEFAULT_PASSWORD);

        $iri = $this->getFirstProductIri();

        $this->client->request(Request::METHOD_PUT, $iri, [
            'json' => [
                'name' => 'Changed name',
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'name' => 'Changed name',
        ]);
    }

    public function testDeleteProduct(): void
    {
        $this->authenticateClient(UserFixtures::REFERENCE_USER, UserFixtures::DEFAULT_PASSWORD);

        $iri = $this->getFirstProductIri();

        $this->client->request(Request::METHOD_DELETE, $iri);

        $this->assertResponseStatusCodeSame(Response::HTTP_NO_CONTENT);
    }

    private function getFirstProductIri(): string
    {
        return $this->findIriBy(Product::class, [
            'name' => \sprintf(ProductFixtures::NAME, 1),
        ]);
    }
}
