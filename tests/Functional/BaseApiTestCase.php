<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Tests\Functional\Auth\AuthTest;
use Coduo\PHPMatcher\PHPUnit\PHPMatcherAssertions;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\ConsoleOutput;

class BaseApiTestCase extends ApiTestCase
{
    use PHPMatcherAssertions;

    /**
     * @var Loader
     */
    private $fixtureLoader;
    private $entityManager;
    protected $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = static::createClient();
        $this->client->disableReboot();

        $this->entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();
        $this->entityManager->getConnection()->beginTransaction();
        $this->entityManager->getConnection()->setAutoCommit(false);
        $this->afterSetUp();
    }

    public function tearDown(): void
    {
        $this->entityManager->getConnection()->rollback();
        $this->entityManager->close();

        parent::tearDown();
    }

    protected function afterSetUp(): void
    {
    }

    public function getUserFromFixture(): User
    {
        /** @var UserFixtures $userFixtures */
        $userFixtures = $this->getFixtureLoader()->getFixture(UserFixtures::class);

        return $userFixtures->getUser();
    }

    public function getFixtureLoader(): Loader
    {
        if (!$this->fixtureLoader) {
            $this->fixtureLoader = new Loader();
        }

        return $this->fixtureLoader;
    }

    public function loadFixtures(array $fixtures, bool $append = false): void
    {
        $ormExecutor = new ORMExecutor($this->entityManager, new ORMPurger($this->entityManager));

        $consoleLogger = new ConsoleLogger(new ConsoleOutput(ConsoleOutput::VERBOSITY_DEBUG));
        $ormExecutor->setLogger(function ($message) use ($consoleLogger) {
            $consoleLogger->info($message);
        });

        foreach ($fixtures as $fixture) {
            $this->getFixtureLoader()->addFixture($fixture);
        }

        $ormExecutor->execute($this->fixtureLoader->getFixtures(), $append);
    }

    protected function authenticateClient(string $username, string $password): void
    {
        $response = $this->client->request(Request::METHOD_POST, AuthTest::AUTH_URI, [
            'json' => [
                'username' => $username,
                'password' => $password,
            ],
        ]);

        $this->client->setDefaultOptions(['auth_bearer' => $response->toArray()['token']]);
    }
}
