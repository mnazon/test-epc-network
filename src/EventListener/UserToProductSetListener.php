<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Security\Core\Security;

class UserToProductSetListener
{
    public function __construct(
        private Security $security
    ) {}

    public function onEntityCreate(ViewEvent $viewEvent): void
    {
        $method = $viewEvent->getRequest()->getMethod();
        $entity = $viewEvent->getControllerResult();

        if ($entity instanceof Product && $method === Request::METHOD_POST) {
            $entity->setUser($this->security->getUser());
        }
    }
}
