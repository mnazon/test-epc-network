<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public const NAME = 'Super product № %s';
    private const COUNT = 10;

    public function getDependencies(): array
    {
        return [
            UserFixtures::class
        ];
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < self::COUNT; $i++) {
            $entity = (new Product())
                ->setName(sprintf(self::NAME, $i))
                ->setPrice(100)
                ->setUser($this->getReference(UserFixtures::REFERENCE_USER));

            $manager->persist($entity);

            $this->addReference(sprintf('%s_%d', 'products', $i), $entity);
        }

        $manager->flush();
    }
}
