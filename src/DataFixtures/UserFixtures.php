<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public const REFERENCE_USER = 'test_user';
    public const DEFAULT_PASSWORD = 'password';
    private const DEFAULT_PASSWORD_HASH = '$2y$13$oSgHdrWl9dl4fMS54huzBuzwP/uVjMENa1Qn2IXjOm4X4SUJNCzNS';

    private $user;

    public function load(ObjectManager $manager): void
    {
        $this->user = (new User())
            ->setUsername(self::REFERENCE_USER)
            ->setPassword(self::DEFAULT_PASSWORD_HASH)
        ;

        $manager->persist($this->user);
        $this->setReference(self::REFERENCE_USER, $this->user);

        $manager->flush();
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
