<?php

declare(strict_types=1);

namespace App\Handler;

use App\Dto\ChangePasswordRequest;
use App\Dto\ChangePasswordResponse;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;

class ChangePasswordRequestHandler
{
    public function __construct(
        private Security $security,
        private EntityManagerInterface $entityManager,
        private UserPasswordHasherInterface $passwordEncoder,
        private JWTTokenManagerInterface $JWTManager
    ) {}

    public function handle(ChangePasswordRequest $request): ChangePasswordResponse
    {
        /** @var User $user */
        $user = $this->security->getUser();
        if (!$user) {
            throw new \RuntimeException('Auth required');
        }

        $user->setPassword(
            $this->passwordEncoder->hashPassword($user, $request->getNewPassword())
        );
        $user->setPasswordChangedAt(new \DateTimeImmutable());

        $this->entityManager->flush();

       return new ChangePasswordResponse(
           $this->JWTManager->create($user)
       );
    }
}
