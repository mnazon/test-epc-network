<?php

declare(strict_types=1);

namespace App\Controller;

use App\Dto\ChangePasswordRequest;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\ChangePasswordResponse;
use App\Handler\ChangePasswordRequestHandler;

class ChangePassword
{
    public function __construct(
        private ValidatorInterface $validator,
        private ChangePasswordRequestHandler $handler
    ) {}

    public function __invoke(ChangePasswordRequest $data): ChangePasswordResponse
    {
        $this->validator->validate($data);

        return $this->handler->handle($data);
    }
}
