<?php

declare(strict_types=1);

namespace App\Dto;

class ChangePasswordRequest
{
    public function __construct(
        private ?string $password = null,
        private ?string $newPassword = null
    ) {}

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getNewPassword(): ?string
    {
        return $this->newPassword;
    }
}
