<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\Product;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ProductVoter extends Voter
{
    private const CREATE = 'CREATE';
    private const EDIT = 'EDIT';
    private const DELETE = 'DELETE';

    public function __construct(
        private AuthorizationCheckerInterface $authorizationChecker
    ) {}

    protected function supports(string $attribute, $subject): bool
    {
        return \in_array($attribute, [self::CREATE, self::EDIT, self::DELETE], true)
            && $subject instanceof Product;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        return match ($attribute) {
            self::CREATE => $this->canCreate(),
            self::EDIT => $this->canEdit($subject, $user),
            self::DELETE => $this->canDelete($subject, $user),
            default => false,
        };
    }

    private function canCreate(): bool
    {
        return $this->authorizationChecker->isGranted(User::ROLE_API_USER);
    }

    private function canEdit(Product $product, UserInterface $user): bool
    {
        return $product->getUser() === $user;
    }

    private function canDelete(Product $product, UserInterface $user): bool
    {
        return $product->getUser() === $user;
    }
}
