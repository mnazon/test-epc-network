<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class TokenAuthenticator extends JWTTokenAuthenticator
{
    public function getUser($preAuthToken, UserProviderInterface $userProvider): User
    {
        /** @var User $user */
        $user = parent::getUser(
            $preAuthToken,
            $userProvider
        );

        $passwordChangedAtTimestamp = $user->getPasswordChangedAt()?->getTimestamp();
        if ($passwordChangedAtTimestamp && $preAuthToken->getPayload()['iat'] < $passwordChangedAtTimestamp) {
            throw new ExpiredTokenException();
        }

        return $user;
    }
}
